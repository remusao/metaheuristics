\section{Recuit simulé}

\subsection{Rappel du problème}

\paragraph{}
Le problème du cas discret consiste à configurer de manière optimale le
placement de composants électroniques. Il faut placer 25 blocs et le seul
mouvement autorisé est la permutation de deux blocs. La fonction objectif
est évaluée en sommant les longueurs de Manhattan des connexions entre les
blocs.

\paragraph{}
Dans le cas de la configuration optimale, la fonction objectif vaut 200.



\subsection{Principe de la méthode}

\paragraph{}
Pour résoudre ce problème, nous avons décidé d'utiliser la méthode du
\emph{recuit simulé}. Nous avons implémenté l'algorithme en Python.

\begin{center}
\begin{algorithm}[H]
 \SetAlgoLined
 X $\leftarrow$ initX()\;
 t $\leftarrow$ initT()\;
 \While{les conditions d'arrêt ne sont pas satisfaites}{
  candidat $\leftarrow$ voisin(X)\;
  e\_candidat $\leftarrow$ énergie(candidat)\;
  $\Delta E$ $\leftarrow$ e\_candidat - énergie(X)\;
  \eIf{$\Delta E < 0$}{
   acceptation du candidat\;
   }{
   acceptation avec probabilité $e^{-\frac{\Delta E}{t}}$\;
  }
  \If{toutes les variables de $X$ ont été modifiées au moins une fois}{
   diminution de la température\;
   }
 }
 \caption{Algorithme du recuit simulé}
\end{algorithm}
\end{center}


\paragraph{Fonctionnement}
\og~Le recuit simulé est une méthode empirique (métaheuristique) inspirée d'un processus utilisé en métallurgie. On alterne dans cette dernière des cycles de refroidissement lent et de réchauffage (recuit) qui ont pour effet de minimiser l'énergie du matériau. Cette méthode est transposée en optimisation pour trouver les extrema d'une fonction.[\ldots] La méthode vient du constat que le refroidissement naturel de certains métaux ne permet pas aux atomes de se placer dans la configuration la plus solide. La configuration la plus stable est atteinte en contrôlant le refroidissement et en le ralentissant par un apport de chaleur externe.~\fg -- Wikipédia.


\paragraph{}
Nous allons donc parcourir l'espace des solution de manière pseudo-aléatoire. Lorsqu'un candidat
diminue l'énergie de la solution, on le garde. Mais il peut arriver que l'on accepte également
une solution moins bonne, avec une certaine probabilité $e^{-\frac{\Delta E}{t}}$ qui dépend donc
de la variation d'énergie par rapport à la solution courante et de la température. Il faut remarquer
que lorsque la température est très élevée, cette distribution de probabilité se comporte comme
une \emph{loi uniforme}, autorisant ainsi les candidats potentiels à parcourir l'ensemble de l'espace
des solutions, ceci permet de ne pas rester bloqué dans des optimum locaux. Lorsque la température
tend vers zéro, la distribution tend progressivement vers
un Dirac centré sur l'optimum de la fonction objectif, n'autorisant ainsi que les candidats
optimum à être retenus en tant que solution. La figure [\ref{fig:temp}] illustre ce phénomène.

\begin{figure}
\includegraphics[width=0.8\textwidth]{temp}
\caption{Évolution des solutions retenues en fonction de la température}
\label{fig:temp}
\end{figure}

\paragraph{Choix du candidat}
La fonction \emph{voisin}, qui effectue une modification élémentaire
sur l'individu $X$, consiste à permuter deux blocs choisis aléatoirement.

\paragraph{Température initiale}
Le choix de la température initiale est un problème difficile, car cette
valeur dépend de chaque problème. Néanmoins, nous savons que la valeur
doit être assez élevée pour que la loi de probabilité $e^{-\frac{\Delta E}{t}}$
se comporte comme une loi uniforme, c'est-à-dire que nous devons avoir autant
de variations négatives que positives pour l'énergie. Nous avons donc choisi
de chercher une telle valeur de la température :

\begin{center}
\begin{algorithm}[H]
 \KwResult{température initiale optimale}
 \SetAlgoLined
 t $\leftarrow$ 1\;
 \While{$e^{-\frac{\Delta E}{t}}$ n'est pas uniforme}{
  t $\leftarrow t \times 2$\;
 }
 \caption{Algorithme du choix de la température initiale}
\end{algorithm}
\end{center}


\paragraph{Diminution de température}
La température ne doit pas diminuer à chaque itération de l'algorithme.
Une solution plus raisonnable consiste à la diminuer une fois que toutes
les variables de notre individu ont été modifiées au moins une fois. Dans
notre cas, il suffit d'attendre que toutes les cases aient été échangées
au moins une fois. De plus, afin que l'efficacité du recuit soit optimale,
il faut une diminution très lente de la température (\emph{logarithmique)} :
$$\boxed{t \leftarrow t \times 0.99}$$
Néanmoins, la vitesse de refroidissement dépend également du problème traité.
Des versions plus évoluées du recuit existent afin de palier à ce problème,
notamment le \emph{recuit simulé adaptatif}, qui permet de modifier la vitesse
de refroidissement en cours d'exécution de l'algorithme.


\paragraph{Condition d'arrêt}
Puisque nous connaissons la valeur de l'optimum global de notre fonction objectif,
nous nous arrêtons lorsque cette valeur est atteinte. Si toutefois nous n'avions
pas connaissance de cette valeur, il serait toujours possible de s'arrêter lorsque
le système n'arrive plus à monter en énergie (sur $k$ itérations successives).


\paragraph{Condition d'acceptation}
Pour l'acceptation des \og~mauvaises~\fg mutations, nous utilisons la règle de Metropolis,
qui consiste à accepter un candidat avec une probabilité $e^{-\frac{\Delta E}{t}}$.

\subsection{Résultats}

\paragraph{}
Lors du lancement du programme, nous tirons au hasard une instance de l'espace des
solutions pour initialiser l'état de notre problème.

\begin{figure}
    \begin{center}
        \includegraphics[scale=0.8]{exo1_1.png}
        \caption{État des blocs au lancement.}
    \end{center}
\end{figure}

\paragraph{}
Dans tout les cas, le recuit simulé semble converger vers la solution optimale
du problème. Grâce aux choix de notre température, le taux de mutation est
assez important pour nous éviter de tomber dans des minimums locaux.
Finalement, le score de notre problème semble suivre une courbe décroissante
tout au long du processus du recuit simulé.

\begin{figure}
    \begin{center}
        \includegraphics[scale=0.8]{exo1_2.png}
        \caption{État des blocs au lancement après quelques itérations.}
    \end{center}
\end{figure}

\paragraph{}
Arrivé à une certaine température, le recuit simulé n'accepte plus de mutations et ne
gardent uniquement que les modifications qui vont améliorer l'état du
problème.

\begin{figure}
    \begin{center}
        \includegraphics[scale=0.8]{exo1_3.png}
        \caption{État des blocs à la fin.}
    \end{center}
\end{figure}

\paragraph{}
Nous atteignons la solution optimale après plus de 25000 itérations. Après
plusieurs lancement on peut remarquer qu'il existe d'autres solutions
optimales comme la suivante :

\begin{figure}
    \begin{center}
        \includegraphics[scale=0.8]{exo1_4.png}
        \caption{Autre solution optimale.}
    \end{center}
\end{figure}
