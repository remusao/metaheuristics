
from scipy.constants import pi
import numpy as np

# -----------
# Michalewicz
# -----------

def mz(dna):
    res = 0.0
    for i in range(0, dna.shape[0]):
        res += np.sin(dna[i]) * (np.sin((i + 1) * dna[i] * dna[i] / pi) ** 20)
    return res

# ----------
# De Jong F1
# ----------

def djf1(dna):
    return np.dot(dna, dna)

# ----------
# De Jong F2
# ----------

def djf2(dna):
    return (100 * (dna[1] * dna[1] - dna[0]) + (1 - dna[0]))

# ----------
# De Jong F3
# ----------

def djf3(dna):
    return np.sum(dna.astype(np.int))

# -------------------
# Goldstein and Price
# -------------------

def gp(x):
    return ((1 + ((x[0] + x[1] + 1) ** 2) * (19 - 14 * x[0] + 13 * (x[0] ** 2) - 14 * x[1] + 6 * x[0] * x[1] + 3 * (x[1] ** 2)))
            * (30 + ((2 * x[0] - 3 * x[1]) ** 2) * (18 - 32 * x[0] + 12 * (x[0] ** 2) - 48 * x[1] - 36 * x[0] * x[1] + 27 * (x[1] ** 2))))

# ----------
# Rosenbrock
# ----------

def r(x):
    res= 0.0
    for i in range(0, x.shape[0] - 1):
        res += 100 * ((x[i] ** 2 - x[i + 1]) ** 2) + (x[i] - 1)
    return res

# --------
# Zakharov
# --------

# TODO

# --------
# Schwefel
# --------

def sh(x):
    return np.sum(-x * np.sin(np.sqrt(np.abs(x))))


