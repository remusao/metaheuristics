
from operator import itemgetter
import numpy as np
import numpy.random as rnd


class ParticleSwarm():

    def __init__(self, dimension):
        assert dimension > 0

        # Init defaults options
        self.nb_particles= 300
        self.max_epoc = 100
        self.begin = -5
        self.end = 5

        # ParticleSwarm parameters
        self.omega = 0.9 # inertia
        self.phi_p = 1.2
        self.phi_g = 0.3

        self.dimension = dimension

    def initSwarm(self, fitness):
        swarm = []

        random_particle = lambda: rnd.random(self.dimension) * (self.end - self.begin) + self.begin
        random_velocity = lambda: rnd.random(self.dimension) * 2 * (self.end - self.begin) - (self.end - self.begin)
        for i in range(0, self.nb_particles):
            new_p= random_particle()
            new_score = fitness(new_p)
            new_v = random_velocity()
            swarm.append((new_score, (new_p, new_v, (new_p, new_score))))

        self.swarm = swarm


    def run(self, fitness, target):

        # Init random swarm
        self.initSwarm(fitness)

        # Find best particle
        score, (x, v, best_known) = min(self.swarm, key = itemgetter(0))
        best = x

        # Find optimum
        epoc = 0
        while epoc < self.max_epoc and not target(score):
            print('Epoc:', epoc, 'Best:', score)
            best, score = self.step(fitness, best, score)
            epoc += 1

        return epoc, score, best


    def step(self, fitness, best, best_score):

        new_swarm = []
        for score, particle in self.swarm:
            x, velocity, best_known = particle
            r = rnd.random(2)

            # Update velocity
            velocity = self.omega * velocity + (self.phi_p * r[0]) * (best_known[0] - x) + (self.phi_g * r[1]) * (best - x)

            # Update particle position
            # TODO : x must remains in the search space
            x = (x + velocity) % self.end

            # Update score
            new_score = fitness(x)

            # Update best known score for the particle
            if new_score < best_known[1]:
                best_known = x, new_score

            # Update global best score
            if new_score < best_score:
                best, best_score = x, new_score

            new_swarm.append((new_score, (x, velocity, best_known)))

        # Update swarm
        self.swarm = new_swarm

        return best, best_score
