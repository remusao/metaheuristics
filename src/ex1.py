#! /usr/bin/python

from __future__ import print_function, absolute_import, division
from simulated_annealing import SimulatedAnnealing
from optimisation_composants import *

import numpy as np

def main():
    sa = SimulatedAnnealing()
    sa.t_init = 15
    solution = sa.run(init_fun, cost_fun, mod_fun, stop_fun)
    print(solution)

if __name__ == '__main__':
    main()
