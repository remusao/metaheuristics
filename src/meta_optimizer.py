#! /usr/bin/python

from optimisation_func import djf1
from genetic_algorithm import GeneticAlgorithm

counter = 0

def fitness(options):
    global counter
    print(counter)
    counter += 1

    selection_amount, mutation_rate = options

    # Create new heuristique algorithm
    ga = GeneticAlgorithm(3)

    # Set options
    ga.begin_range = -5.12
    ga.end_range = 5.12

    ga.selection_amount = int(selection_amount) + 1
    ga.mutation_rate = mutation_rate

    # Find solution
    epoc, score, best = ga.run(djf1, lambda s: abs(s) <= 1e-3)
    return epoc



def main():

    # Create new heuristique algorithm
    ga = GeneticAlgorithm(2)

    # Set options
    ga.max_epoc = 20
    ga.pop_size = 30
    ga.selection_amount = 10
    ga.begin_range = 0.0
    ga.end_range = 15.0
    ga.mutation_rate = 0.4

    # Find solution
    epoc, score, best = ga.run(fitness, lambda s: False)
    print(best)



if __name__ == '__main__':
    main()
