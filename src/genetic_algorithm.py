
from __future__ import print_function, absolute_import, division

from operator import itemgetter
from itertools import chain
import random

def argsort(seq):
    return sorted(xrange(len(seq)), key=seq.__getitem__)

# DNA = tuple(code, score)

# Functions on DNA


def newRandom(dna_size, end):
    new = range(end)
    random.shuffle(new)
    return new


def newCrossover(parents):
    size = len(parents[0])
    new = [0] * size
    indices = map(argsort, parents)
    print(indices)
    print(parents)
    for i in range(0, size):
        index = random.randint(0, len(parents) - 1)
        new[i] = parents[index][indices[index][i]]
    return new, 0


def mutate(dna, rate, end):
    dna, score = dna
    for i in range(0, len(dna)):
        if random.random() <= rate:
            a, b = random.sample(xrange(end), 2)
            dna[a], dna[b] = dna[b], dna[a]
    return dna, score


# Genetic algorithm functions


# Class wrapper
class GeneticAlgorithm(object):

    def __init__(self, dna_size):
        assert dna_size > 0

        # Init defaults options
        self.pop_size = 1000
        self.selection_amount = 100
        self.max_epoc = 100000
        self.mutation_rate = 0.5
        self.begin_range = -5
        self.end_range = 5

        self.dna_size = dna_size


    def initPop(self):
        self.population = [(newRandom(self.dna_size, self.end_range), 0) for i in range(0 , self.pop_size)]


    def run(self, fitness, target):

        epoc = 0

        # Init population
        self.initPop()

        # Init best score
        best, score = self.population[0]
        score = fitness(best)

        while epoc < self.max_epoc and not target(score):
            print('epoc', epoc, 'score', score)
            best, score = self.step(fitness, best, score)
            epoc += 1

        return epoc, score, best


    def step(self, fitness, best, best_score):

        # Update fitness score
        self.population = [(dna, fitness(dna)) for dna, score in self.population]

        # Sort by score
        self.population.sort(key = itemgetter(1))

        # Update best DNA
        if self.population[0][1] < best_score:
            best, best_score = self.population[0]

        # Create new generation
        elite = [dna for dna, score in self.population[:self.selection_amount]]
        #elite = list(chain(elite, [newRandom(self.dna_size, self.end_range) for i in range(0, self.selection_amount)]))
        self.population = [mutate(newCrossover(elite), self.mutation_rate, self.end_range) for i in range(0, self.pop_size)]

        return best, best_score
