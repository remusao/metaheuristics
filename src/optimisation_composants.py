
from __future__ import print_function
import random
try:
    import numpypy as np
except:
    import numpy as np


def init_fun():
    new = np.arange(25)
    random.shuffle(new)
    return new


def manhattan(x1, y1, x2, y2):
    return abs(x1 - x2) + abs(y1 - y2)


def cost_fun(cases):
    cost = 0
    visited = np.zeros((25, 25), np.bool)

    for i, a in enumerate(cases):

        if (i - 5) / 5 >= 0 and not visited[cases[i - 5], a]:
            b = cases[i - 5]
            cost += manhattan(a % 5, a / 5, b % 5, b / 5)
            visited[a, b] = True
            visited[b, a] = True

        if i % 5 != 0 and not visited[cases[i - 1], a]:
            b = cases[i - 1]
            cost += manhattan(a % 5, a / 5, b % 5, b / 5)
            visited[a, b] = True
            visited[b, a] = True

        if (i + 5) / 5 <= 4 and not visited[cases[i + 5], a]:
            b = cases[i + 5]
            cost += manhattan(a % 5, a / 5, b % 5, b / 5)
            visited[a, b] = True
            visited[b, a] = True

        if (i + 1) % 5 != 0 and not visited[cases[i + 1], a]:
            b = cases[i + 1]
            cost += manhattan(a % 5, a / 5, b % 5, b / 5)
            visited[a, b] = True
            visited[b, a] = True

    return cost * 5


def mod_fun(cases):
    # We want to swap 2 elements (positions) of the list "cases".
    a, b = random.sample(xrange(25), 2)

    new = cases.copy()
    new[a], new[b] = cases[b], cases[a]

    return new


def stop_fun(score):
    return score == 200


