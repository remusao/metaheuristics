from math import exp
import random
try:
    import numpypy as np
except:
    import numpy as np


class SimulatedAnnealing(object):

    def __init__(self):

        # Init default options
        self.t_init = None
        self.t_step = 0.999
        self.t_stop = 10e-5
        self.max_epoc = 1000000


    def findOptimalTemperature(self, mod_fun, cost_fun, init_fun):
        print('Looking for optimal starting temperature')
        t = 10
        energie_eq = 1
        nb_step = 1000
        while energie_eq != nb_step:
            t *= 2
            # Try to get a uniform law
            cases = init_fun()
            energie_eq = 0
            for i in range(0, nb_step):
                cases_mod = mod_fun(cases.copy())
                mod_e = cost_fun(cases_mod) - cost_fun(cases)
                if random.random() < exp(-mod_e / t):
                    energie_eq += 1
                else:
                    energie_eq -= 1
            print(t, energie_eq)
        print('Optimal temperature is:', t)

        return t


    def run(self, init_fun, cost_fun, mod_fun, stop_fun):

        # Find optimal starting temperature
        if not self.t_init:
            t = self.findOptimalTemperature(mod_fun, cost_fun, init_fun)
        else:
            t = self.t_init

        # Initial state
        cases = init_fun()
        cases_score = cost_fun(cases)
        changed = np.zeros(cases.shape, np.bool)

        epoc = 0
        while (not stop_fun(cases_score)) and epoc < self.max_epoc and t > self.t_stop:
            # Choose neighbor
            cases_mod = mod_fun(cases)

            changed = np.logical_or(changed, cases_mod != cases)

            # Compute score
            score_mod = cost_fun(cases_mod)
            # Compute energy delta
            mod_e = score_mod - cases_score

            # Keep the candidate or not
            if mod_e <= 0 or random.random() < exp(-mod_e / t):
                cases = cases_mod
                cases_score = score_mod

            # Change temperature if all variables have been modified
            if changed.all():
                t *= self.t_step
                changed = np.zeros(cases.shape, np.bool)

            epoc += 1
            print("Iteration number: %i; mod_e: %d; cost: %d, temp: %d" % (epoc, mod_e, cases_score, t))

        return cases
